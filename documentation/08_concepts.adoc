[[section-concepts]]

== Cross-cutting Concepts

=== Scaling of Components

|===
^|MLCVZoo Model ^|CPU Training ^|GPU Training ^|Multi-GPU Training ^| CPU Inference ^| GPU Inference

|DarknetDetectionModel
^|X
^|X
^|?
^|X
^|X

|===