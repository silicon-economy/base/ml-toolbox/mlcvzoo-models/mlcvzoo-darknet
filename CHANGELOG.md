# MLCVZoo mlcvzoo_darknet module Versions:

4.0.2 (2024-07-02):
------------------
Updated links in pyproject.toml

4.0.1 (2023-05-24):
-------------------
Relicense to OLFL-1.3 which succeeds the previous license

4.0.0 (2023-02-14):
------------------
Implement API changes introduced by mlcvzoo-base version 5.0.0
- Remove detector-config and use the feature of the single ModelConfiguration
- Remove duplicate attributes

3.1.1 (2022-09-09):
------------------
Ensure ConfigBuilder version 7 compatibility

3.1.0 (2022-08-08):
------------------
Adapt to mlcvzoo-base 4.0.0

3.0.1 (2022-07-11):
------------------
Prepare package for PyPi

3.0.0 (2022-05-16):
------------------
Use new features from AnnotationClassMapper that have been added with mlcvzoo_base v3.0.0

2.0.0 (2022-04-05)
------------------
- initial release of the package
