FROM nexus.apps.sele.iml.fraunhofer.de/sele/ml-toolbox/cicd/mlcvzoo-ci-image:latest

# Define paths. Let all path definitions always end with a '/'!

# setup development environment:
ENV BUILD_ENV_DIR="/build-env/"
ENV PROJECT_DIR="${BUILD_ENV_DIR}MLCVZoo/mlcvzoo-darknet/"
# External Projects:
ENV EXTERNAL_DIR="${BUILD_ENV_DIR}external/"

RUN mkdir -p "$PROJECT_DIR" "$EXTERNAL_DIR"

# ====================================================================
# Checkout and compile darknet
# NOTE: Since darknet is written in C it needs to be compiled. They provided a "darknet.py"
#       scipt, which provides an python wrapper. It is used in the mlcvzoo to infer darknet models.

# set environment variables for compiling darknet:
ENV DARKNET_DIR="${EXTERNAL_DIR}darknet/"
ARG DARKNET_COMMIT_SHA="b8dceb7ed055b1ab2094bdbd0756b61473db3ef6"
COPY scripts/gen_darknet_archs.sh "$EXTERNAL_DIR/"
RUN git clone https://github.com/AlexeyAB/darknet.git "$DARKNET_DIR" && \
    cd "$DARKNET_DIR" && \
    git reset --hard "$DARKNET_COMMIT_SHA" && \
    make -j8 GPU=1 CUDNN=1 CUDNN_HALF=1 OPENCV=1 LIBSO=1 \
        AVX=$([ "x$(uname -m)" = "xaarch64" ] && echo 0 || echo 1) \
        ARCH="$(/bin/bash $EXTERNAL_DIR/gen_darknet_archs.sh $TORCH_CUDA_ARCH_LIST)"

COPY pyproject.toml "$PROJECT_DIR"
COPY poetry.lock "$PROJECT_DIR"

WORKDIR "$PROJECT_DIR"

ENV VIRTUAL_ENV="${BUILD_ENV_DIR}venv"
ENV PATH="${VIRTUAL_ENV}bin:$PATH"
RUN python3 -m venv "$VIRTUAL_ENV" \
  && poetry run pip install --upgrade pip \
  && poetry install --no-interaction --no-ansi --no-root

# Cleanup poetry cache
RUN rm -rf ~/.cache/pypoetry

# We don't deliver unit tests so even without --no-root we would need to set the PYTHONPATH here
ENV PYTHONPATH="$PYTHONPATH:$PROJECT_DIR"

# Cleanup poetry cache
RUN rm -rf ~/.cache/pypoetry

# ====================================================================
# Label the image
LABEL org.opencontainers.image.authors="Maximilian Otten <maximilian.otten@iml.fraunhofer.de>, Christian Hoppe <christian.hoppe@iml.fraunhofer.de" \
      org.opencontainers.image.vendor="Fraunhofer IML" \
      org.opencontainers.image.title="MLCVZoo Darknet - Main GPU-enabled gitlab-runner container" \
      org.opencontainers.image.description="Container image for GPU enabled integration testing and continuous delivery for MLCVZoo Darknet"
