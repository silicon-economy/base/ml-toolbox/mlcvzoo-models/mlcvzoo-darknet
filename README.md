# MLCVZoo Darknet

The MLCVZoo is an SDK for simplifying the usage of various (machine learning driven)
computer vision algorithms. The package **mlcvzoo_darknet** is the wrapper module for the
[darknet framework](https://github.com/AlexeyAB/darknet).

## Install
`
pip install mlcvzoo-darknet
`

## Technology stack

- Python
