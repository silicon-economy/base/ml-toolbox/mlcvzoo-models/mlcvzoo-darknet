# Copyright Open Logistics Foundation
#
# Licensed under the Open Logistics Foundation License 1.3.
# For details on the licensing terms, see the LICENSE file.
# SPDX-License-Identifier: OLFL-1.3

import logging
import os
from typing import cast
from unittest import main

import cv2
from mlcvzoo_base.api.data.bounding_box import BoundingBox
from mlcvzoo_base.api.data.box import Box
from mlcvzoo_base.api.data.class_identifier import ClassIdentifier
from related import to_model

from mlcvzoo_darknet.configuration import DarknetConfig
from mlcvzoo_darknet.model import DarknetDetectionModel
from mlcvzoo_darknet.tests.test_template import TestTemplate

logger = logging.getLogger(__name__)


class TestDarknet(TestTemplate):
    def test_configuration(self):
        configuration = DarknetDetectionModel.create_configuration(
            from_yaml=os.path.join(
                self.project_root,
                "test_data",
                "test_darknet",
                "yolov4_coco",
                "yolov4_darknet_coco.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        assert configuration is not None

        configuration_2 = cast(
            DarknetConfig, to_model(DarknetConfig, configuration.to_dict())
        )
        assert configuration_2 is not None

    def test_yolov4_no_darknet_dir_error(self):
        with self.assertRaises(ValueError):
            darknet_dir = self.string_replacement_map.pop("DARKNET_DIR")

            DarknetDetectionModel(
                from_yaml=os.path.join(
                    self.project_root,
                    "test_data",
                    "test_darknet",
                    "yolov4_coco",
                    "yolov4_darknet_coco.yaml",
                ),
                string_replacement_map=self.string_replacement_map,
            )

            self.string_replacement_map["DARKNET_DIR"] = darknet_dir

    def test_yolov4_no_darknet_module_error(self):
        with self.assertRaises(ValueError):
            darknet_model = DarknetDetectionModel(
                from_yaml=os.path.join(
                    self.project_root,
                    "test_data",
                    "test_darknet",
                    "yolov4_coco",
                    "yolov4_darknet_coco.yaml",
                ),
                string_replacement_map=self.string_replacement_map,
                init_for_inference=False,
            )

            darknet_model.predict(
                data_item=os.path.join(
                    self.project_root,
                    "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
                )
            )

    def test_num_classes(self):
        darknet_model = DarknetDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "test_data",
                "test_darknet",
                "yolov4_coco",
                "yolov4_darknet_coco.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        assert darknet_model.num_classes == 80

    def test_yolov4_training(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST training of yolov4-detector:\n"
            "#      test_yolov4_training(self)\n"
            "############################################################"
        )

        model = DarknetDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "test_data",
                "test_darknet",
                "yolov4_coco",
                "yolov4_darknet_coco.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        model.train()

        weights_path = os.path.join(
            model.get_training_output_dir(),
            f"{model.unique_name}_final.weights",
        )

        if not os.path.exists(weights_path):
            raise ValueError(
                "Training failed, not final weights file has been produced at "
                f"'{weights_path}'"
            )

    def test_yolov4_no_annotations(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST training of yolov4-detector:\n"
            "#      test_yolov4_training(self)\n"
            "############################################################"
        )

        model = DarknetDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "test_data",
                "test_darknet",
                "yolov4_coco",
                "yolov4_darknet_coco.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        model.configuration.train_config.train_annotation_handler_config.pascal_voc_input_data = (
            []
        )

        with self.assertRaises(ValueError):
            model.train()

    def test_yolov4_training_without_optional_configs(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST training of yolov4-detector:\n"
            "#      test_yolov4_training_withoutTrainAnnotationHandlerConfig(self)\n"
            "############################################################"
        )

        model = DarknetDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "test_data",
                "test_darknet",
                "yolov4_coco",
                "yolov4_darknet_coco.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
        )

        model.configuration.train_config.train_annotation_handler_config.write_output.darknet_train_set = (
            None
        )
        with self.assertRaises(ValueError):
            model.train()

        model.configuration.train_config.train_annotation_handler_config.write_output = (
            None
        )
        with self.assertRaises(ValueError):
            model.train()

        model.configuration.train_config.train_annotation_handler_config = None
        with self.assertRaises(ValueError):
            model.train()

    def test_yolov4_inference_on_path(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST inference of yolov4-detector:\n"
            "#      test_yolov4_inference(self)\n"
            "############################################################"
        )

        logger.debug("test_yolov4_inference(): Create DarknetDetectionModel")
        model = DarknetDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "test_data",
                "test_darknet",
                "yolov4_coco",
                "yolov4_darknet_coco.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        test_image_path = os.path.join(
            self.project_root,
            "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
        )

        _, bounding_boxes = model.predict(data_item=test_image_path)
        logger.debug("test_yolov4_inference(): model.predict() returned")

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED BOUNDING-BOXES: \n"
            " %s\n"
            "\n==============================================================\n",
            bounding_boxes,
        )

        # NOTE: Depending how darknet is compiled, we get different results for the coordinates.
        #       Therefore, the coordinates are allowed to be in range
        allowed_margin = 10

        expected_bounding_box_0 = BoundingBox(
            box=Box(xmin=2005, ymin=1732, xmax=3327, ymax=2359),
            class_identifier=ClassIdentifier(
                class_id=43,
                class_name="knife",
            ),
            score=0.8271,
            difficult=False,
            occluded=False,
            content="",
        )

        is_correct_0 = (
            expected_bounding_box_0.box.xmin - allowed_margin
            <= bounding_boxes[0].box.xmin
            <= expected_bounding_box_0.box.xmin + allowed_margin
            and expected_bounding_box_0.box.ymin - allowed_margin
            <= bounding_boxes[0].box.ymin
            <= expected_bounding_box_0.box.ymin + allowed_margin
            and expected_bounding_box_0.box.xmax - allowed_margin
            <= bounding_boxes[0].box.xmax
            <= expected_bounding_box_0.box.xmax + allowed_margin
            and expected_bounding_box_0.box.ymax - allowed_margin
            <= bounding_boxes[0].box.ymax
            <= expected_bounding_box_0.box.ymax + allowed_margin
            and expected_bounding_box_0.class_id == bounding_boxes[0].class_id
            and expected_bounding_box_0.class_name == bounding_boxes[0].class_name
        )

        expected_bounding_box_1 = BoundingBox(
            box=Box(xmin=228, ymin=701, xmax=1962, ymax=2205),
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.9936,
            difficult=False,
            occluded=False,
            content="",
        )

        is_correct_1 = (
            expected_bounding_box_1.box.xmin - allowed_margin
            <= bounding_boxes[1].box.xmin
            <= expected_bounding_box_1.box.xmin + allowed_margin
            and expected_bounding_box_1.box.ymin - allowed_margin
            <= bounding_boxes[1].box.ymin
            <= expected_bounding_box_1.box.ymin + allowed_margin
            and expected_bounding_box_1.box.xmax - allowed_margin
            <= bounding_boxes[1].box.xmax
            <= expected_bounding_box_1.box.xmax + allowed_margin
            and expected_bounding_box_1.box.ymax - allowed_margin
            <= bounding_boxes[1].box.ymax
            <= expected_bounding_box_1.box.ymax + allowed_margin
            and expected_bounding_box_1.class_id == bounding_boxes[1].class_id
            and expected_bounding_box_1.class_name == bounding_boxes[1].class_name
        )

        if not is_correct_0:
            logger.error(
                "Found wrong bounding_box: \n"
                "  Expected 0:  %s\n"
                "  Predicted 0: %s\n",
                expected_bounding_box_0,
                bounding_boxes[0],
            )

            raise ValueError("Output is not valid!")

        if not is_correct_1:
            logger.error(
                "Found wrong bounding_box: \n"
                "  Expected 1:  %s\n"
                "  Predicted 1: %s\n",
                expected_bounding_box_1,
                bounding_boxes[1],
            )

            raise ValueError("Output is not valid!")

    def test_yolov4_inference_on_image(self) -> None:
        logger.info(
            "############################################################\n"
            "# TEST inference of yolov4-detector:\n"
            "#      test_yolov4_inference(self)\n"
            "############################################################"
        )

        logger.debug("test_yolov4_inference(): Create DarknetDetectionModel")
        model = DarknetDetectionModel(
            from_yaml=os.path.join(
                self.project_root,
                "test_data",
                "test_darknet",
                "yolov4_coco",
                "yolov4_darknet_coco.yaml",
            ),
            string_replacement_map=self.string_replacement_map,
            init_for_inference=True,
        )

        test_image_path = os.path.join(
            self.project_root,
            "test_data/images/test_inference_task/test_object-detection_inference_image.jpg",
        )

        logger.debug("test_yolov4_inference(): Read image")
        test_image = cv2.imread(test_image_path)
        logger.debug("test_yolov4_inference(): model.predict()")

        _, bounding_boxes = model.predict(data_item=test_image)
        logger.debug("test_yolov4_inference(): model.predict() returned")

        logger.debug(
            "\n==============================================================\n"
            "PREDICTED BOUNDING-BOXES: \n"
            " %s\n"
            "\n==============================================================\n",
            bounding_boxes,
        )

        # NOTE: Depending how darknet is compiled, we get different results for the coordinates.
        #       Therefore, the coordinates are allowed to be in range
        allowed_margin = 10

        expected_bounding_box_0 = BoundingBox(
            box=Box(xmin=2005, ymin=1732, xmax=3327, ymax=2359),
            class_identifier=ClassIdentifier(
                class_id=43,
                class_name="knife",
            ),
            score=0.8271,
            difficult=False,
            occluded=False,
            content="",
        )

        is_correct_0 = (
            expected_bounding_box_0.box.xmin - allowed_margin
            <= bounding_boxes[0].box.xmin
            <= expected_bounding_box_0.box.xmin + allowed_margin
            and expected_bounding_box_0.box.ymin - allowed_margin
            <= bounding_boxes[0].box.ymin
            <= expected_bounding_box_0.box.ymin + allowed_margin
            and expected_bounding_box_0.box.xmax - allowed_margin
            <= bounding_boxes[0].box.xmax
            <= expected_bounding_box_0.box.xmax + allowed_margin
            and expected_bounding_box_0.box.ymax - allowed_margin
            <= bounding_boxes[0].box.ymax
            <= expected_bounding_box_0.box.ymax + allowed_margin
            and expected_bounding_box_0.class_id == bounding_boxes[0].class_id
            and expected_bounding_box_0.class_name == bounding_boxes[0].class_name
        )

        expected_bounding_box_1 = BoundingBox(
            box=Box(xmin=228, ymin=701, xmax=1962, ymax=2205),
            class_identifier=ClassIdentifier(
                class_id=41,
                class_name="cup",
            ),
            score=0.9936,
            difficult=False,
            occluded=False,
            content="",
        )

        is_correct_1 = (
            expected_bounding_box_1.box.xmin - allowed_margin
            <= bounding_boxes[1].box.xmin
            <= expected_bounding_box_1.box.xmin + allowed_margin
            and expected_bounding_box_1.box.ymin - allowed_margin
            <= bounding_boxes[1].box.ymin
            <= expected_bounding_box_1.box.ymin + allowed_margin
            and expected_bounding_box_1.box.xmax - allowed_margin
            <= bounding_boxes[1].box.xmax
            <= expected_bounding_box_1.box.xmax + allowed_margin
            and expected_bounding_box_1.box.ymax - allowed_margin
            <= bounding_boxes[1].box.ymax
            <= expected_bounding_box_1.box.ymax + allowed_margin
            and expected_bounding_box_1.class_id == bounding_boxes[1].class_id
            and expected_bounding_box_1.class_name == bounding_boxes[1].class_name
        )

        if not is_correct_0:
            logger.error(
                "Found wrong bounding_box: \n"
                "  Expected 0:  %s\n"
                "  Predicted 0: %s\n",
                expected_bounding_box_0,
                bounding_boxes[0],
            )

            raise ValueError("Output is not valid!")

        if not is_correct_1:
            logger.error(
                "Found wrong bounding_box: \n"
                "  Expected 1:  %s\n"
                "  Predicted 1: %s\n",
                expected_bounding_box_1,
                bounding_boxes[1],
            )

            raise ValueError("Output is not valid!")


if __name__ == "__main__":
    main()
